package manament
import org.zkoss.zk.grails.composer.*
import org.zkoss.zk.ui.Executions 
import org.zkoss.zk.ui.select.annotation.Wire
import org.zkoss.zk.ui.select.annotation.Listen
import org.zkoss.zul.ListModel;

class EditfoodComposer extends zk.grails.Composer {

    def afterCompose = { window ->
  
    	 $('#logo').on('click', {
            redirect(uri:'/loginmamager.zul')  
        })

         $('#back').on('click',{

            redirect(uri:'/edit.zul')                   
        })


         $('#namefood').focus()
        int countlist = 1
        for(Editfood m : Editfood.findAll()){
            $('#listnamemode > rows').append{
                row{
                    label(value:"${countlist}")
                    label(value:""+m.namefood1)
                    label(value:""+m.costfood1)
                    label(value:""+m.typefood1)
                    button(id:m.namefood1,mold:"trendy")
                }
            }
             countlist++
            $('#listnamemode > rows > row > button').on('click',{
                def delete = Editfood.findByNamefood1(it.target.id)
                delete.delete()
                redirect(uri:'/editfood.zul')

              
                })
           
            }



        $('#okfood').on('click',{
        	if($('#namefood').val() == "" || $('#costfood').val() == "" ){
        		alert("กรอกข้อมูลไม่ครบครับ!")}
        			
          	else{

		            		def add = new Editfood()
		            		add.typefood1 =$('#typefood').text()
		            		add.namefood1 =$('#namefood').text()
		            		add.costfood1 = Integer.parseInt($('#costfood').text())
		            		add.save()
                             redirect(uri:'/editfood.zul')

            				}

      
        })
        

        $('#clear').on('click',{
        	 redirect(uri:'/editfood.zul')
        })

    }
}

