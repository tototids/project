package manament

import org.zkoss.zk.grails.composer.*
import org.zkoss.zk.ui.Executions 
import org.zkoss.zk.ui.select.annotation.Wire
import org.zkoss.zk.ui.select.annotation.Listen
import org.zkoss.zul.ListModel;
import static java.util.Calendar.*

class Add2Composer extends zk.grails.Composer {

    def afterCompose = { window ->
        
        	 $('#logo').on('click', {
            redirect(uri:'/loginmamager.zul')  
        })

         $('#back').on('click',{

            redirect(uri:'/stock.zul')                   
        })





         $('#save1').on('click',{

             if($('#typefood2').text() == "" || 
                $('#namefood').text() == "" ||
                $('#addfood').text() == "" ||
                $('#sv1').text() == "" ||
                $('#pricefoodcosts').text() == "" ||
                $('#pricefood').text() == "" ||
                $('#dateimportfood').text() == "" ||
                $('#extdatfood').text() == ""){
                alert("กรอกข้อมูลไม่ครบครับ!")
             }

             else{ 

                    def ad = new Addfood()

                        ad.typefood1 = $('#typefood2').text()
                        ad.namefood1 = $('#namefood').text()
                        ad.numberfood1 = Integer.parseInt($('#addfood').text())
                        ad.stylefood1 = $('#sv1').text()
                        ad.pricefoodcosts1 = Double.parseDouble($('#pricefoodcosts').text())
                        ad.priceperunitfood1 = Double.parseDouble($('#pricefood').text())
                        ad.dateimportfood1 = $('#dateimportfood').getValue() 
                        ad.extdatfood1 = $('#extdatfood').getValue() 
                        ad.save()
                        redirect(uri:'/add2.zul')

             }
                          
        })

$('#namefood').focus()
        for(Addfood m : Addfood.findAll()){
            $('#listproduct1 > rows').append{
                row{
                    label(value:""+m.typefood1)
                    label(value:""+m.namefood1)
                    label(value:""+m.numberfood1)
                    label(value:""+m.stylefood1)
                    label(value:""+m.priceperunitfood1)
                    label(value:""+m.dateimportfood1)
                    label(value:""+m.extdatfood1)
                }
            }
           
        }






         $('#cancle1').on('click',{

            redirect(uri:'/add2.zul')                   
        })


    }
}
