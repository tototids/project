package manament

import org.zkoss.zk.grails.composer.*
import org.zkoss.zk.ui.Executions 
import org.zkoss.zk.ui.select.annotation.Wire
import org.zkoss.zk.ui.select.annotation.Listen
import org.zkoss.zul.ListModel;

class EditdrinkComposer extends zk.grails.Composer {

    def afterCompose = { window ->
       
    	 $('#logo').on('click', {
            redirect(uri:'/loginmamager.zul')  
        })

         $('#back').on('click',{

            redirect(uri:'/edit.zul')                   
        })

 $('#namedrink').focus()
        int countlist1 = 1
        for(Editdrink n : Editdrink.findAll()){
            $('#listnamemode1 > rows').append{
                row{
                    label(value:"${countlist1}")
                    label(value:""+n.namedrink1)
                    label(value:""+n.costdrink1)
                    label(value:""+n.typedrink1)
                    button(id:n.namedrink1,mold:"trendy")
                }
            }
             countlist1++
            $('#listnamemode1 > rows > row > button').on('click',{
                def delete = Editdrink.findByNamedrink1(it.target.id)
                delete.delete()
              redirect(uri:'/editdrink.zul')
            })
           
        }





        $('#okdrink').on('click',{
            if($('#namedrink').val() == "" || $('#costdrink').val() == "" ){
                alert("กรอกข้อมูลไม่ครบครับ!")}
                    
      

            else{

                            def add = new Editdrink()
                            add.typedrink1 =$('#tpyedrink').text()
                            add.namedrink1 =$('#namedrink').text()
                            add.costdrink1 = Integer.parseInt($('#costdrink').text())
                            add.save()
                            redirect(uri:'/editdrink.zul') 
                            }
  
    })

        $('#cleardrink').on('click',{
            redirect(uri:'/editdrink.zul') 
        })


    }

}