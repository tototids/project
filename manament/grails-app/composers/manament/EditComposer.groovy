package manament


class EditComposer extends zk.grails.Composer {

    def afterCompose = { window ->

    	 $('#logo').on('click', {
            redirect(uri:'/loginmamager.zul')  
        })

         $('#back').on('click',{

            redirect(uri:'/managment.zul')                   
        })

         $('#foodedit').on('click', {
            redirect(uri:'/editfood.zul')  
        })

         $('#drinkedit').on('click', {
            redirect(uri:'/editdrink.zul')  
        })
 

    }
}
